import React from 'react'

function Footer() {
    // Create the logic about the date Today
    const today = new Date();
    return (
        <footer>
            <p>Copyright &copy; {today.getFullYear()}</p>
        </footer>
    )
}

export default Footer