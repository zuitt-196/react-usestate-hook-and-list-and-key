import React from 'react'
import { useState } from 'react';
import { FaTrashAlt } from 'react-icons/fa'


const Content = () => {
    const [items, setItems] = useState([
        {
            id: 1,
            checked: true,
            item: "One half pound bag of Cocoa Covered Almonds Unsalted"
        },
        {
            id: 2,
            checked: false,
            item: "Item 2"
        },
        {
            id: 3,
            checked: false,
            item: "Item 3"
        }
    ]);

    const handleCkeck = (id) => {
        // declaration of varible with the value high order function which is map() method.
        // Along the use of Ternary conditiond
        const listItems = items.map((item) => item.id === id ? { ...item, checked: !item.checked } : item);
        setItems(listItems);
        localStorage.setItem('shoopingList', JSON.stringify(listItems));

    }
    const handleDelete = (id) => {
        // Use the high order function which filter() method which is trgger the item.id which have came to condition of 
        //Strict inequality to result the false boolean 
        const listItems = items.filter((item) => item.id !== id);
        // console.log(listItems);
        setItems(listItems);
        localStorage.setItem('shoopingList', JSON.stringify(listItems));
    }

    return (

        <main>
            {/* By adding Expression of jsx have condition used of Ternary condition which is to define if the id item list having length or value is it execute true in ternary,  while the default false if ever has no length or value it will be execute false */}
            {items.length ? (
                < ul >
                    {items.map((item) => (
                        <li className="item" key={item.id}>
                            {/* use by anonymous function with the fuinction Expression with handleCkeck and the argument of key method of list  */}
                            <input type="checkbox" onChange={() => handleCkeck(item.id)} checked={item.checked} />
                            <label
                                style={(item.checked) ? { textDecoration: 'line-through' } : null}
                                onDoubleClick={() => handleCkeck(item.id)}>
                                {item.item}</label>
                            < FaTrashAlt
                                onClick={() => handleDelete(item.id)}
                                role='button' tabIndex="0" />
                        </li>
                    ))}


                </ul>

            ) : (
                <p style={{ marginTop: '2rem' }}>Your list is Empty</p>
            )
            }



        </main >

    )
}

export default Content



